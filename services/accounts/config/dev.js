const prodConfig = require('./prod');

const config = Object.assign({}, prodConfig)

config.jwtSecret = 'myjwtsecert123';

config.mongoDbUrl = process.env.MONGO_DB_URL || 'mongodb://localhost:27017/accounts';

module.exports = config;
