const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const fs = require('fs');
const rfs = require('rotating-file-stream');

//
// Dependencies
//

const logger = require('./src/helpers/logger');
const config = require('./config');

//
// Express web server
//

const health = require('./src/health')();

const router = express.Router();
router.use('/api/health', health);

const app = express();
app.disable('x-powered-by');
app.use(morgan('dev', { skip: function (req, res) { return res.statusCode < 400 } }))
app.use(morgan('common', { stream: rfs('access.log', { interval: '1d', maxFiles: 7, path: config.logPath }) }));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(config.baseUrl, router);

fs.existsSync(config.logPath) || fs.mkdirSync(config.logPath);

const server = app.listen(config.port, () => {
  logger.info('Serving on port %s', config.port);
});

//
// Background tasks
//

// require the database library (which instantiates a connection to mongodb)
require('./src/helpers/db');

//
// General process hooks
//

process.on('SIGTERM', function () {
  server.close(function () {
    process.exit(0);
  });
});

process.on('uncaughtException', function (err) {
  logger.error('Caught exception: ', err.stack || err);
  process.exit(1);
});
