# selfserve-marketplace [![pipeline status](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/badges/master/pipeline.svg)](https://gitlab.com/selfserve-marketplace/selfserve-marketplace/commits/master)

Selfserve Marketplace mono-repo.

## Development Cycle

* Install the development requirements
  * `npm install -g nodemon concurrently wait-on`
* Run `scripts/run_dev.sh`

## Additional Information

### Ports Mapping

| Service | Port |
| :---: | :---: |
| Accounts | 3000 |
| Inventory | 3010 |
