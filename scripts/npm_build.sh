#!/bin/bash

BUILD_DIR=$(pwd)

for SERVICE_PATH in ./services/*/; do
  SERVICE_NAME=$(basename $SERVICE_PATH)

  echo "*** Building $SERVICE_NAME service ***"

  cd $SERVICE_PATH
  npm install
  npm prune
  cd $BUILD_DIR
done
