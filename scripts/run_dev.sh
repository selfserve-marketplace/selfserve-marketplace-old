#!/bin/bash

export NODE_ENV=dev

concurrently \
  -n mongodb,accounts,inventory \
  -c "bgMagenta.bold,bgRed.bold,bgBlue.bold" \
  --kill-others \
  "docker run -p 27017:27017 mongo:4.1.6 > /dev/null || true" \
  "wait-on tcp:27017 && nodemon services/accounts/index.js" \
  "wait-on tcp:27017 && nodemon services/inventory/index.js"
