#!/bin/bash

for SERVICE_PATH in ./services/*/; do
  SERVICE_NAME=$(basename $SERVICE_PATH)

  echo "*** Performing Docker release for $SERVICE_NAME service ***"

  SERVICE_DOCKER_IMAGE="$DOCKER_IMAGE_BASE/$SERVICE_NAME:$VERSION"
  docker build -t $SERVICE_DOCKER_IMAGE $SERVICE_PATH
  docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  docker push $SERVICE_DOCKER_IMAGE
done
